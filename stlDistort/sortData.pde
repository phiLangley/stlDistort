void sortVerts(){

  println("original... " + vList.size());

  for (int i = 0; i< vList.size(); i++){  
    for (int j = 0; j< vList.size(); j++){
      if (i!=j){
        if (vList.get(i).equals(vList.get(j))){
          vList.set(j, "duplicate");
        }
      }
    } 
  }
  
  for (int i = vList.size()-1; i>=0 ; i--){  
    if (vList.get(i).equals("duplicate")){
      vListIndex.remove(i);
    }
  }
  
  println("sorted... " + vListIndex.size());
  
/////////////CONVERT ARRAY LIST OF STRINGS TO FLOAT ARRAY  
  vertices = new Float[vListIndex.size()][3];
  verticesBase = new Float[vListIndex.size()][3];
  
  for (int i=0; i<vListIndex.size(); i++){
    String temp[] = split(vListIndex.get(i), ",");
    vertices[i][0] = float(temp[0]);
    vertices[i][1] = float(temp[1]);
    vertices[i][2] = float(temp[2]);
    verticesBase[i][0] = float(temp[0]);
    verticesBase[i][1] = float(temp[1]);
    verticesBase[i][2] = float(temp[2]);
    
    if (boundX[0]>vertices[i][0]){
      boundX[0] = vertices[i][0];
    }
    if (boundX[1]<vertices[i][0]){
      boundX[1] = vertices[i][0];
    }
    if (boundY[0]>vertices[i][1]){
      boundY[0] = vertices[i][1];
    }
    if (boundY[1]<vertices[i][1]){
      boundY[1] = vertices[i][1];
    }   
    if (boundZ[0]>vertices[i][2]){
      boundZ[0] = vertices[i][2];
    }
    if (boundZ[1]<vertices[i][2]){
      boundZ[1] = vertices[i][2];
    }
    
  }
  
  boundX[0] = boundX[0] * STLscale;
  boundX[1] = boundX[1] * STLscale;
  boundY[0] = boundY[0] * STLscale;
  boundY[1] = boundY[1] * STLscale;
  boundZ[0] = boundZ[0] * STLscale;
  boundZ[1] = boundZ[1] * STLscale;
  
  println("X... " + boundX[0] + " ... " + boundX[1]);
  println("Y... " + boundY[0] + " ... " + boundY[1]);
  println("Z... " + boundZ[0] + " ... " + boundZ[1]);
  
}

void sortFaces(){
  
  String tempFaceIndex;
  int tempIndex;
    
  for (int i = 0; i<fList.size(); i++){   
    tempFaceIndex = ""; 
    String tempVert[] = split(fList.get(i), "#");   
    for (int j=0; j<3; j++){     
      tempIndex=0;    
      for (int k=0; k<vListIndex.size(); k++){       
        if(tempVert[j].equals(vListIndex.get(k))){
          tempIndex = k;
        }            
      }      
      tempFaceIndex = tempFaceIndex + tempIndex + " ";      
    }    
    fListIndex.add(trim(tempFaceIndex));       
  }
  
/////////////CONVERT ARRAY LIST OF STRINGS TO INTEGER ARRAY
  faces = new int[fListIndex.size()][3];
  for (int i=0; i<fListIndex.size(); i++){
    String temp[] = split(fListIndex.get(i), " ");
    faces[i][0] = int(temp[0]);
    faces[i][1] = int(temp[1]);
    faces[i][2] = int(temp[2]);
  }
  
}

