////a class for a draggable slider - these ones control the parameter values
class buttonSlider{
  int index;
  int posX=0;
  int posY=0;
  int dimX=0;
  int dimY=sliderHeight;
  String name;
  float val= map(dimX, 0, screenGUI, 0,0.05);
  float pVal;
  Boolean active=true;
  
  buttonSlider(int i){
    index=i;
    posY = (index+1)*sliderHeight;
    name = "param-"+str(i);
  }
  
  void update(){
    pVal = val;
    dimX = mouseX;
    val = map(dimX, 0, screenGUI, 0,0.05);
  }
  
  void display(){   
   
    //println(dimX); 
    
    fill(255);
    strokeWeight(1);
    stroke(0);
    rectMode(CORNER);
    rect(posX,posY,dimX, dimY);
    
    fill(255,0,0);
    textAlign(LEFT);
    textFont(font, 10);
    text(name,posX+5,posY+(sliderHeight/2)+2);
    
    fill(255,0,0);
    textAlign(RIGHT);
    textFont(font, 10);
    text(str(val),screenGUI-5,posY+(sliderHeight/2)+2);
    
  }
  
}

//////////////////////////////////////////////////////////////////
void initializeSliders(){
  for (int i=0; i<activeSliders; i++){
    buttonSlide[i] = new buttonSlider(i);
  }
  
 buttonSlide[0].name = "xDistort";
 buttonSlide[1].name = "yDistort";
 buttonSlide[2].name = "zDistort";
}


