float ROTX = 3.14;                        
float ROTY = 1.5;
float SCALE = 1;

void mouseDragged() {
  
   if ((mouseX>screenGUI)||(mouseY>(activeSliders+1)*sliderHeight)){
     if (mouseButton == LEFT) {
       ROTX += (mouseX - pmouseX) * 0.01;
       ROTY -= (mouseY - pmouseY) * 0.01;     }
     if (mouseButton == RIGHT) {
       if (mouseY>pmouseY){
          SCALE = SCALE+0.1;
       } else {
          SCALE = SCALE-0.1;
       }
       if (SCALE<0.1){ SCALE=0.1;}
       if (SCALE>=10){ SCALE=10;}
     }
     redraw();
   }

  if ((mouseX<screenGUI)&&(mouseY>sliderHeight)&&(buttonClick[0].active==true)&&(mouseX>0)&&(mouseY<(activeSliders+1)*sliderHeight)){
     int button = int(mouseY/sliderHeight)-1;
     buttonSlide[button].update();  
     updateVerts=true;
     if (button == 0) {updateX=true;}
     if (button == 1) {updateY=true;}
     if (button == 2) {updateZ=true;}
     redraw();  
   }
   
}

void mouseReleased(){
   if ((mouseX<screenGUI)&&(mouseY<sliderHeight)){
     buttonClick[0].update();  
     redraw();  
   }
}



