class faceObj{

  int v0;
  int v1;
  int v2;
  
  faceObj(int vert0, int vert1, int vert2){    
     v0 = vert0;
     v1 = vert1;
     v2 = vert2;   
   }
    
  void display(){  
    beginShape();
      vertex(vertices[v0][0] * STLscale, vertices[v0][1] * STLscale, vertices[v0][2] * STLscale);
      vertex(vertices[v1][0] * STLscale, vertices[v1][1] * STLscale, vertices[v1][2] * STLscale);
      vertex(vertices[v2][0] * STLscale, vertices[v2][1] * STLscale, vertices[v2][2] * STLscale);
    endShape(CLOSE);     
  }
 
}
/////////////////////////////////////////////////////////////

void initializeFaces(){
      
    for (int i = 0; i <fListIndex.size(); i++){
      face = new faceObj(faces[i][0], faces[i][1], faces[i][2]);
      faceList.add(face);  
    }   
    println("initialized");
    
}

