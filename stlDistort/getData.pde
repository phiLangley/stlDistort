void loadStlData(){
/////////////////////////LOAD STL FILE  
  String lines[] = loadStrings(txtSTL);
  
/////////////////////////MAKE A TABLE OF FACES
  fTable = new Table();
  fTable.addColumn("x0");
  fTable.addColumn("y0");
  fTable.addColumn("z0");
  fTable.addColumn("x1");
  fTable.addColumn("y1");
  fTable.addColumn("z1");
  fTable.addColumn("x2");
  fTable.addColumn("y2");
  fTable.addColumn("z2");
  
  for (int i = 0; i< lines.length; i++){
    if (lines[i].equals("outer loop")) {         
      TableRow newRow =fTable.addRow();
      String tempFace0[] = split(lines[i+1], " ");
      newRow.setFloat("x0", float(tempFace0[1]));
      newRow.setFloat("y0", float(tempFace0[2]));
      newRow.setFloat("z0", float(tempFace0[3]));   
      String tempFace1[] = split(lines[i+2], " ");
      newRow.setFloat("x1", float(tempFace1[1]));
      newRow.setFloat("y1", float(tempFace1[2]));
      newRow.setFloat("z1", float(tempFace1[3]));    
      String tempFace2[] = split(lines[i+3], " ");
      newRow.setFloat("x2", float(tempFace2[1]));
      newRow.setFloat("y2", float(tempFace2[2]));
      newRow.setFloat("z2", float(tempFace2[3]));    
    }
  }
  saveTable(fTable, "data/fTable.csv");
  
/////////////////////////MAKE A TABLE OF VERTICES
  vTable = new Table();
  vTable.addColumn("x");
  vTable.addColumn("y");
  vTable.addColumn("z");
  
  for (int i = 0; i< lines.length; i++){    
    String tempVert[] = split(lines[i], " "); 
    if (tempVert[0].equals("vertex")) {    
      TableRow newRow = vTable.addRow();      
      newRow.setFloat("x", float(tempVert[1]));
      newRow.setFloat("y", float(tempVert[2]));
      newRow.setFloat("z", float(tempVert[3]));      
      vList.add(tempVert[1]+","+tempVert[2]+","+tempVert[3]);
      vListIndex.add(tempVert[1]+","+tempVert[2]+","+tempVert[3]);
    }
  } 
  saveTable(vTable, "data/vTable.csv");

  for (int i= 0; i< vList.size(); i+=3){
    fList.add(vList.get(i) + "#" + vList.get(i+1) + "#" + vList.get(i+2));
  }
  
}
