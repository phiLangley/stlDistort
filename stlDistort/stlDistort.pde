/////////////////////////GLOBAL SCREEN VARIABLES
int screenH = 600;
int screenW = 800;
int screenGUI = 200;
/////////////////////////STL VARIABLES
String txtSTL = "data/monkey.stl";
int STLscale = 100;
/////////////////////////SLIDERS
int sliderHeight=15;
int activeSliders=3;
buttonSlider[] buttonSlide = new buttonSlider[activeSliders];
buttonClicker[] buttonClick = new buttonClicker[1];
PFont font;
boolean updateVerts = false;
boolean updateX = false;
boolean updateY = false;
boolean updateZ = false;
/////////////////////////DATA TABLES
Table fTable;
Table vTable;
Table sortVertTable;
/////////////////////////LISTS FOR SORTING FACES AND VERICES
ArrayList<String> fList;
ArrayList<String> fListIndex;
ArrayList<String> vList;
ArrayList<String> vListIndex;
IntList vListDelete;
/////////////////////////ARRAYS FOR STORING GEOMETRY
Float vertices[][];
Float verticesBase[][];
int faces[][];
/////////////////////////FACE OBJECTS
ArrayList<faceObj> faceList;
faceObj face;
/////////////////////////BOUNDING BOX DIMS
Float[] boundX = new Float[2];
Float[] boundY = new Float[2];
Float[] boundZ = new Float[2];

void setup() {

 noLoop();
 size(screenW, screenH,OPENGL);
 
 font = loadFont("Verdana-10.vlw");
 initializeSliders();
 initializeButtons();
 
 fList = new ArrayList();
 fListIndex = new ArrayList();
 vList = new ArrayList();
 vListIndex = new ArrayList();
 vListDelete = new IntList();
 
 faceList = new ArrayList<faceObj>();
 
 boundX[0] = 16000000.0;
 boundX[1] = -16000000.0;
 boundY[0] = 16000000.0;
 boundY[1] = -16000000.0;
 boundZ[0] = 16000000.0;
 boundZ[1] = -16000000.0;
 
 loadStlData();
 sortVerts();
 sortFaces();
 initializeFaces();

}

void draw() {
  
  background(0);
  
  if (updateVerts==true){
    updateVerts();  
  }
   
  pushMatrix();
  
    directionalLight(125, 125, 125, 0, 1, -1);
    ambientLight(75,75,75);
    
    translate(width/2,height/2,0);

    rotateY(ROTX);
    rotateX(ROTY);
    scale(SCALE);
    
    noFill();
    strokeWeight(1/SCALE);
    stroke(255); 
    drawMesh();
    noLights();

  popMatrix();

  if (buttonClick[0].active==true){
    for (int i=0; i<activeSliders; i++){
      buttonSlide[i].display();
    }
  } 
  buttonClick[0].display(); 
}
